import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from './logo.svg';
import './App.css';
import { BtcUsdtChart } from './Components/BtcUsdtChart';

function App() {
  return (
    <div className="App">
      <div className='container'>
        <BtcUsdtChart />
      </div>
    </div>
  );
}

export default App;
