import React, { useEffect, useState } from "react";
import { createChart, IChartApi, ISeriesApi } from "lightweight-charts";

interface Item {
  date: number,
  price: number
}

export const BtcUsdtChart = () => {
  const [chart, setChart] = useState<IChartApi | null>(null);
  const [candlestickSeries, setCandlestickSeries] = useState<ISeriesApi<"Candlestick"> | null>(null);
  const [data, setData] = useState<Item[]>([]);
  

  useEffect(() => {
console.log('useeffect1');

    console.log(chart);

    console.log('setChart');

    setChart(createChart(document.getElementById("chart")!, {
      width: 600,
      height: 300,
      layout: {
        //backgroundColor: "#253248",
        textColor: "rgba(255, 255, 255, 0.9)",
      },
      grid: {
        vertLines: {
          color: "#334158",

        },
        horzLines: {
          color: "#334158",
        },
      },
      crosshair: {
        mode: 0,
      },
      overlayPriceScales: {
        borderColor: "#485c7b"
      },
      timeScale: {
        borderColor: "#485c7b",
      },
    }));
  }, []);

  useEffect(() => {
    console.log('useeffect2');
    if (chart) {
      let newCandlestickSeries = chart.addCandlestickSeries();
      fetch("https://api.binance.com/api/v3/klines?symbol=BTCUSDT&interval=1d")
        .then((res) => res.json())
        .then((data) => {
          const prices = data.map((d: any) => ({
            time: d[0] / 1000,
            open: parseFloat(d[1]),
            high: parseFloat(d[2]),
            low: parseFloat(d[3]),
            close: parseFloat(d[4]),
          }));
          newCandlestickSeries?.setData(prices);
        });
      console.log('setCandlestickSeries');
      setCandlestickSeries(newCandlestickSeries);
    }
  }, [chart]);

  return <div id="chart" />;
};
